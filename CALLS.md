## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Cisco CML. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Cisco CML.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Cisco_cml. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiUiAuthenticationAuthenticate(body, callback)</td>
    <td style="padding:15px">Authenticate to the system, get a web token</td>
    <td style="padding:15px">{base_path}/{version}/authenticate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersUserRolesGet(callback)</td>
    <td style="padding:15px">Get roles for this user</td>
    <td style="padding:15px">{base_path}/{version}/user/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersUserListGet(callback)</td>
    <td style="padding:15px">Get the list of available users</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersUserPasswordPut(userId, body, callback)</td>
    <td style="padding:15px">User password change.</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/change_password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersUserInfoGet(userId, callback)</td>
    <td style="padding:15px">Gets user info</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersUserDelete(userId, callback)</td>
    <td style="padding:15px">Deletes user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersUserPost(userId, body, callback)</td>
    <td style="padding:15px">Creates user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingStatusGet(callback)</td>
    <td style="padding:15px">Get current licensing configuration and status</td>
    <td style="padding:15px">{base_path}/{version}/licensing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingTechSupportGet(callback)</td>
    <td style="padding:15px">Get current licensing tech support</td>
    <td style="padding:15px">{base_path}/{version}/licensing/tech_support?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingStatusPut(callback)</td>
    <td style="padding:15px">Renew licensing authorization with the backend</td>
    <td style="padding:15px">{base_path}/{version}/licensing/authorization/renew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingTransportPut(body, callback)</td>
    <td style="padding:15px">Setup licensing transport configuration</td>
    <td style="padding:15px">{base_path}/{version}/licensing/transport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingDevCaPost(body, callback)</td>
    <td style="padding:15px">Setup a licensing public certificate for internal deployment of an unregistered product instance</td>
    <td style="padding:15px">{base_path}/{version}/licensing/certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingDevCaDelete(callback)</td>
    <td style="padding:15px">Clear any licensing public certificate for internal deployment of an unregistered product instance</td>
    <td style="padding:15px">{base_path}/{version}/licensing/certificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingRegistrationPost(body, callback)</td>
    <td style="padding:15px">Setup licensing registration</td>
    <td style="padding:15px">{base_path}/{version}/licensing/registration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingRegistrationPut(callback)</td>
    <td style="padding:15px">Request a renewal of licensing registration against current SSMS</td>
    <td style="padding:15px">{base_path}/{version}/licensing/registration/renew?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingRegistrationDelete(callback)</td>
    <td style="padding:15px">Request deregistration from the current SSMS</td>
    <td style="padding:15px">{base_path}/{version}/licensing/deregistration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingFeaturesGet(callback)</td>
    <td style="padding:15px">Get current licensing features</td>
    <td style="padding:15px">{base_path}/{version}/licensing/features?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingFeaturesPatch(body, callback)</td>
    <td style="padding:15px">Update current licensing feature(s)</td>
    <td style="padding:15px">{base_path}/{version}/licensing/features?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingReservationModePut(body, callback)</td>
    <td style="padding:15px">Enable or disable reservation mode in unregistered agent.</td>
    <td style="padding:15px">{base_path}/{version}/licensing/reservation/mode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingReservationRequestPost(callback)</td>
    <td style="padding:15px">Initiate reservation by generating request code and message to the user.</td>
    <td style="padding:15px">{base_path}/{version}/licensing/reservation/request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingReservationCompletePost(body, callback)</td>
    <td style="padding:15px">Complete reservation by installing authorization code from CSSM.</td>
    <td style="padding:15px">{base_path}/{version}/licensing/reservation/complete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingReservationCancelDelete(callback)</td>
    <td style="padding:15px">Cancel reservation request without completing it.</td>
    <td style="padding:15px">{base_path}/{version}/licensing/reservation/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingReservationReleaseDelete(callback)</td>
    <td style="padding:15px">Return a completed reservation</td>
    <td style="padding:15px">{base_path}/{version}/licensing/reservation/release?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingReservationDiscardDelete(body, callback)</td>
    <td style="padding:15px">Discard a reservation authorization code for an already cancelled reservation request.</td>
    <td style="padding:15px">{base_path}/{version}/licensing/reservation/discard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingReservationConfirmationCodeGet(callback)</td>
    <td style="padding:15px">Get the confirmation code</td>
    <td style="padding:15px">{base_path}/{version}/licensing/reservation/confirmation_code?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingReservationConfirmationCodeDelete(callback)</td>
    <td style="padding:15px">Remove the confirmation code</td>
    <td style="padding:15px">{base_path}/{version}/licensing/reservation/confirmation_code?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingReservationReturnCodeGet(callback)</td>
    <td style="padding:15px">Get the return code</td>
    <td style="padding:15px">{base_path}/{version}/licensing/reservation/return_code?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLicensingReservationReturnCodeDelete(callback)</td>
    <td style="padding:15px">Remove the return code</td>
    <td style="padding:15px">{base_path}/{version}/licensing/reservation/return_code?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersSystemArchive(callback)</td>
    <td style="padding:15px">Download an archive of user-created system data</td>
    <td style="padding:15px">{base_path}/{version}/system_archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersClearBackupsAndShutdown(callback)</td>
    <td style="padding:15px">Remove backups</td>
    <td style="padding:15px">{base_path}/{version}/clear_backups_and_shutdown?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeDefinitionsGetListUiTransformed(callback)</td>
    <td style="padding:15px">Get simplified definitions of the types of nodes supported by this system.</td>
    <td style="padding:15px">{base_path}/{version}/simplified_node_definitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersWaitForLldConnected(callback)</td>
    <td style="padding:15px">This web service blocks and does not return until the LLD connects to the controller.</td>
    <td style="padding:15px">{base_path}/{version}/wait_for_lld_connected?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeDefinitionSchema(callback)</td>
    <td style="padding:15px">Returns the JSON schema that defines the node definition objects.</td>
    <td style="padding:15px">{base_path}/{version}/node_definition_schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLabSchema(callback)</td>
    <td style="padding:15px">Returns the JSON schema that defines the lab.</td>
    <td style="padding:15px">{base_path}/{version}/lab_schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersFeedbackHandler(callback)</td>
    <td style="padding:15px">Sends feedback to the product team.</td>
    <td style="padding:15px">{base_path}/{version}/feedback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersSystemStatsHandler(callback)</td>
    <td style="padding:15px">Get statistics (e.g., CPU and memory usasge) about the usage of the system's compute nodes.</td>
    <td style="padding:15px">{base_path}/{version}/system_stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersDiagnosticsHandler(callback)</td>
    <td style="padding:15px">Return diagnostic information for the back end systems.</td>
    <td style="padding:15px">{base_path}/{version}/diagnostics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersAuthOk(callback)</td>
    <td style="padding:15px">Check whether the API call is properly authenticated</td>
    <td style="padding:15px">{base_path}/{version}/authok?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetWebSessionTimeoutHandler(callback)</td>
    <td style="padding:15px">Get the Web session timeout in seconds.</td>
    <td style="padding:15px">{base_path}/{version}/web_session_timeout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersSetWebSessionTimeoutHandler(timeout, callback)</td>
    <td style="padding:15px">Set the Web session timeout in seconds.</td>
    <td style="padding:15px">{base_path}/{version}/web_session_timeout/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetMacAddressBlockHandler(callback)</td>
    <td style="padding:15px">Get the MAC address block used on this controller (value from 0-7).</td>
    <td style="padding:15px">{base_path}/{version}/mac_address_block?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersSetMacAddressBlockHandler(block, callback)</td>
    <td style="padding:15px">Set the MAC address block to use (0-7).</td>
    <td style="padding:15px">{base_path}/{version}/mac_address_block/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersSystemInformationHandler(callback)</td>
    <td style="padding:15px">Get information about the system where the application runs. This API call can be called without au</td>
    <td style="padding:15px">{base_path}/{version}/system_information?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersWebsocketStatusHandler(callback)</td>
    <td style="padding:15px">Get information about the web sockets in use by the system.</td>
    <td style="padding:15px">{base_path}/{version}/system/websocket_status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersConvergedLabHandler(labId, callback)</td>
    <td style="padding:15px">Wait for convergence.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/check_if_converged?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersPostNodeHandler(labId, populateInterfaces, body, callback)</td>
    <td style="padding:15px">Add a node to the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLabSimStateHandler(labId, callback)</td>
    <td style="padding:15px">Get the overall simulation state for the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersSimulationStatsHandler(labId, callback)</td>
    <td style="padding:15px">Get information about the specified simulation, such as the amount of CPU its nodes are consuming.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/simulation_stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLabElementStateHandler(labId, callback)</td>
    <td style="padding:15px">Get the state of all nodes, interfaces, and links in the lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/lab_element_state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersSampleLabLoad(labId, callback)</td>
    <td style="padding:15px">Loads Sample Lab</td>
    <td style="padding:15px">{base_path}/{version}/sample/labs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLabEventHandler(labId, callback)</td>
    <td style="padding:15px">Get list of events for the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLabListExtConnHandler(labId, callback)</td>
    <td style="padding:15px">Get list of external connections available for the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/external_connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersUpdateTopologyHandler(labId, body, callback)</td>
    <td style="padding:15px">DEPRECATED  Set the topology for the specified lab_id.</td>
    <td style="padding:15px">{base_path}/{version}/update_lab_topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetPyatsTestbed(labId, hostname, callback)</td>
    <td style="padding:15px">Returns YAML with pyATS testbed for specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/pyats_testbed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLabAllocateAndStart(labId, callback)</td>
    <td style="padding:15px">Start the specified lab as a simulation.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLabStop(labId, callback)</td>
    <td style="padding:15px">Stop the simulation for the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLabUndo(labId, callback)</td>
    <td style="padding:15px">Undo the last event for the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/undo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLabWipe(labId, force, callback)</td>
    <td style="padding:15px">Wipe the persisted state for all nodes in this lab.  (Lab must be stopped before it can be wiped.)</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/wipe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersCreateLabHandler(title, callback)</td>
    <td style="padding:15px">Create a new lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetLabListHandler(callback)</td>
    <td style="padding:15px">Get a list of labs visible to the user.</td>
    <td style="padding:15px">{base_path}/{version}/labs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetLabInfoHandler(labId, callback)</td>
    <td style="padding:15px">Returns details about the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersRemoveLabHandler(labId, callback)</td>
    <td style="padding:15px">Remove the specified lab, if it exists.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersPopulateLabsHandler(callback)</td>
    <td style="padding:15px">Get the list of labs.</td>
    <td style="padding:15px">{base_path}/{version}/populate_lab_tiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetLabTopologyHandler(labId, excludeConfigurations, callback)</td>
    <td style="padding:15px">Get the topology for the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetLabTileHandler(labId, callback)</td>
    <td style="padding:15px">Get the info required to present the lab 'tile' on the main page of the UI.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/tile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersImportNgHandler(title, isJson, body, callback)</td>
    <td style="padding:15px">Create a lab from the specified topology, specified in the CML^2 YAML format (with backwards suppor</td>
    <td style="padding:15px">{base_path}/{version}/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersImport1dotxHandler(title, body, callback)</td>
    <td style="padding:15px">Create a lab from the specified VIRL v1.x topology file contents.</td>
    <td style="padding:15px">{base_path}/{version}/import/virl-1x?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersDownloadLabHandler(labId, callback)</td>
    <td style="padding:15px">Download the lab</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersPutLabNotesHandler(labId, originUuid, body, callback)</td>
    <td style="padding:15px">Set the notes of the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/notes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersPutLabTitleHandler(labId, originUuid, body, callback)</td>
    <td style="padding:15px">Set the title of the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/title?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersPutLabDescriptionHandler(labId, originUuid, body, callback)</td>
    <td style="padding:15px">Set the description of the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/description?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersPostInterfaceHandler(labId, body, callback)</td>
    <td style="padding:15px">Create one or more interfaces.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersPostLinkHandler(labId, body, callback)</td>
    <td style="padding:15px">Create a link or physical connection between the two interfaces in the specified topology.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersConvergedNodeHandler(labId, nodeId, callback)</td>
    <td style="padding:15px">Wait for convergence.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/check_if_converged?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersSampleLabList(callback)</td>
    <td style="padding:15px">Get the list of available sample labs</td>
    <td style="padding:15px">{base_path}/{version}/sample/labs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeDefinitionGetList(callback)</td>
    <td style="padding:15px">Get definitions of the types of nodes supported by this system.</td>
    <td style="padding:15px">{base_path}/{version}/node_definitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetConsoleKeyForLabNode(labId, nodeId, line, callback)</td>
    <td style="padding:15px">Returns the key for the specified console.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/keys/console?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetVncKeyForLabNode(labId, nodeId, callback)</td>
    <td style="padding:15px">Returns the key to access the node via VNC.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/keys/vnc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetLabNodesHandler(labId, callback)</td>
    <td style="padding:15px">Get a list of all of the node IDs in the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetNodeStateHandler(labId, nodeId, callback)</td>
    <td style="padding:15px">Get the state of the specified node in the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersStartNodeHandler(labId, nodeId, callback)</td>
    <td style="padding:15px">Start the specified node in the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/state/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersStopNodeHandler(labId, nodeId, callback)</td>
    <td style="padding:15px">Stop the specified node in the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/state/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersWipeNodeHandler(labId, nodeId, callback)</td>
    <td style="padding:15px">Wipe the presisted disk image for the specified node in the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/wipe_disks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersExtractNodeConfigurationHandler(labId, nodeId, callback)</td>
    <td style="padding:15px">Update the configuration for the specified node in the specified lab from the running node</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/extract_configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeConfigGet(labId, nodeId, callback)</td>
    <td style="padding:15px">Get the initial bootstrap configuration for the specified node.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeConfigPut(labId, nodeId, originUuid, body, callback)</td>
    <td style="padding:15px">Set the initial bootstrap configuration for the specified node.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeTagsGet(labId, nodeId, callback)</td>
    <td style="padding:15px">Get the tags for the specified node.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/tags/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeTagPost(labId, nodeId, tag, callback)</td>
    <td style="padding:15px">Add the tag to the lab/node specified.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/tags/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeTagDelete(labId, nodeId, tag, body, callback)</td>
    <td style="padding:15px">Delete tag for the specified node.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/tags/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeImageDefinitionPut(labId, nodeId, body, callback)</td>
    <td style="padding:15px">Set the image definition for the specified node.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/image_definition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeViewGet(labId, nodeId, simplified, callback)</td>
    <td style="padding:15px">Get the details for the specified node</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeViewPatch(labId, nodeId, originUuid, callback)</td>
    <td style="padding:15px">Update details for the specified node.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeViewDelete(labId, nodeId, callback)</td>
    <td style="padding:15px">Delete the specified node.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetLabNodeLayer3AddressesHandler(labId, nodeId, callback)</td>
    <td style="padding:15px">Return the node's allocated L3 addresses if the node is connected to an L2 external connector (in b</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/layer3_addresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersConvergedLinkHandler(labId, linkId, callback)</td>
    <td style="padding:15px">Wait for convergence.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/links/{pathv2}/check_if_converged?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetPcapKeyForLabLink(labId, linkId, callback)</td>
    <td style="padding:15px">Gets the key or ID for the packet capture running on the specified link.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/links/{pathv2}/capture/key?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersPutLinkCaptureStartHandler(labId, linkId, callback)</td>
    <td style="padding:15px">Start a packet capture on the specified link.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/links/{pathv2}/capture/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersPutLinkCaptureStopHandler(labId, linkId, callback)</td>
    <td style="padding:15px">Stop the packet capture on the specified link.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/links/{pathv2}/capture/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersPutLinkStartHandler(labId, linkId, callback)</td>
    <td style="padding:15px">Start the specified link.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/links/{pathv2}/state/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersPutLinkStopHandler(labId, linkId, callback)</td>
    <td style="padding:15px">Stop the specified link.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/links/{pathv2}/state/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLinkViewGet(labId, linkId, simplified, callback)</td>
    <td style="padding:15px">Get the details for the specified link.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/links/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLinkViewPut(labId, linkId, callback)</td>
    <td style="padding:15px">Set the details for the specified link</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/links/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLinkViewPatch(labId, linkId, callback)</td>
    <td style="padding:15px">Set the details for the specified link</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/links/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersLinkViewDelete(labId, linkId, callback)</td>
    <td style="padding:15px">Delete the specified link</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/links/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeDefinitionsPost(json, body, callback)</td>
    <td style="padding:15px">Create new node definition</td>
    <td style="padding:15px">{base_path}/{version}/node_definitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeDefinitionsPut(body, callback)</td>
    <td style="padding:15px">Update the specified node definition</td>
    <td style="padding:15px">{base_path}/{version}/node_definitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeDefinitionsGet(defId, json, callback)</td>
    <td style="padding:15px">Get the node definition for the specified type of node.</td>
    <td style="padding:15px">{base_path}/{version}/node_definitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeDefinitionsDelete(defId, callback)</td>
    <td style="padding:15px">Remove the specified node definition</td>
    <td style="padding:15px">{base_path}/{version}/node_definitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetLabLayer3AddressesHandler(labId, callback)</td>
    <td style="padding:15px">Return the allocated L3 addresses for all nodes if the node is connected to an L2 external connecto</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/layer3_addresses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeImageDefinitionGet(labId, nodeId, callback)</td>
    <td style="padding:15px">Get the image definition for the specified lab/node.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/image_definition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersBuildConfigurationsManager(labId, callback)</td>
    <td style="padding:15px">Generate configurations for the nodes in the topology.</td>
    <td style="padding:15px">{base_path}/{version}/build_configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeByLabelGet(labId, searchQuery, callback)</td>
    <td style="padding:15px">Search for the node label matching the query within all nodes of the given lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/find/node/label/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersNodeTagFindall(labId, searchQuery, callback)</td>
    <td style="padding:15px">Search for nodes matching the given tags within all labs.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/find_all/node/tag/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersTelemetryDataHandler(callback)</td>
    <td style="padding:15px">Get list of telemetry events</td>
    <td style="padding:15px">{base_path}/{version}/telemetry_data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersDiagnosticEventDataHandler(callback)</td>
    <td style="padding:15px">Get list of diagnostic events</td>
    <td style="padding:15px">{base_path}/{version}/diagnostic_event_data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersSystemHealthGet(callback)</td>
    <td style="padding:15px">Get system health</td>
    <td style="padding:15px">{base_path}/{version}/system_health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetInterfaceStateHandler(labId, interfaceId, callback)</td>
    <td style="padding:15px">Get the state for the specified interface in the specified lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/interfaces/{pathv2}/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetNodeInterfacesHandler(labId, nodeId, data, callback)</td>
    <td style="padding:15px">Get the interfaces for the specified node in the specifid lab.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/nodes/{pathv2}/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersStopInterfaceHandler(labId, interfaceId, callback)</td>
    <td style="padding:15px">Stop the specified interface.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/interfaces/{pathv2}/state/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersStartInterfaceHandler(labId, interfaceId, callback)</td>
    <td style="padding:15px">Start the specified interface</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/interfaces/{pathv2}/state/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersInterfaceViewGet(labId, interfaceId, callback)</td>
    <td style="padding:15px">Get the details for the specified interface</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersInterfaceViewPut(labId, interfaceId, callback)</td>
    <td style="padding:15px">Set the details for the specified interface.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersInterfaceViewPatch(labId, interfaceId, callback)</td>
    <td style="padding:15px">Set the details for the specified interface.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersInterfaceViewDelete(labId, interfaceId, callback)</td>
    <td style="padding:15px">Delete the specified interface.</td>
    <td style="padding:15px">{base_path}/{version}/labs/{pathv1}/interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersImageDefinitionsForNdIdGet(defId, callback)</td>
    <td style="padding:15px">Get the Image Definition for the specified Node Definition</td>
    <td style="padding:15px">{base_path}/{version}/node_definitions/{pathv1}/image_definitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersImageDefinitionDropfolderListGet(callback)</td>
    <td style="padding:15px">Get the list of uploaded images</td>
    <td style="padding:15px">{base_path}/{version}/list_image_definition_drop_folder?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersImageDefinitionsList(callback)</td>
    <td style="padding:15px">Get the list of all Image Definitions</td>
    <td style="padding:15px">{base_path}/{version}/image_definitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersImageDefinitionsPost(json, body, callback)</td>
    <td style="padding:15px">Create new Image Definition</td>
    <td style="padding:15px">{base_path}/{version}/image_definitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersImageDefinitionsPut(body, callback)</td>
    <td style="padding:15px">Update the specified Image Definition</td>
    <td style="padding:15px">{base_path}/{version}/image_definitions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersImageDefinitionsGet(defId, json, callback)</td>
    <td style="padding:15px">Get the specified Image Definition</td>
    <td style="padding:15px">{base_path}/{version}/image_definitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersImageDefinitionsDelete(defId, callback)</td>
    <td style="padding:15px">Remove the specified Image Definition</td>
    <td style="padding:15px">{base_path}/{version}/image_definitions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersDiskImageUpload(body, callback)</td>
    <td style="padding:15px">Upload a disk / reference platform image. The filename must be provided in X-Original-File-Name</td>
    <td style="padding:15px">{base_path}/{version}/images/upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersImageDefinitionDropfolderRemove(filename, callback)</td>
    <td style="padding:15px">Delete a disk / reference platform image.</td>
    <td style="padding:15px">{base_path}/{version}/images/manage/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetAllConsoleKeysHandler(callback)</td>
    <td style="padding:15px">Get the console keys for all consoles.</td>
    <td style="padding:15px">{base_path}/{version}/keys/console?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simpleUiHttpHandlersGetAllVncKeysHandler(callback)</td>
    <td style="padding:15px">Get all keys to access nodes via VNC.</td>
    <td style="padding:15px">{base_path}/{version}/keys/vnc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
