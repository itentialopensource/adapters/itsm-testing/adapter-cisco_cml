# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Cisco_cml System. The API that was used to build the adapter for Cisco_cml is usually available in the report directory of this adapter. The adapter utilizes the Cisco_cml API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Cisco CML adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco CML. With this adapter you have the ability to perform operations on items such as:

- Network Topology
- Traffic

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
