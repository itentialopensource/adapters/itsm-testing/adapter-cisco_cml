
## 0.2.0 [05-23-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_cml!2

---

## 0.1.2 [09-07-2021]

- Authentication and security fixes

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_cml!1

---

## 0.1.1 [03-23-2021]

- Initial Commit

See commit d34aa8c

---
