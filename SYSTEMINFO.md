# Cisco Modeling Labs

Vendor: Cisco Systems
Homepage: https://www.cisco.com/

Product: Modeling Labs
Product Page: https://www.cisco.com/c/en/us/products/cloud-systems-management/modeling-labs/index.html

## Introduction
We classify Cisco CML into the ITSM domain as Cisco CML is a network simulation tool that enables users to design, build, and test network topologies in a virtual environment.

## Why Integrate
The Cisco CML adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco CML. With this adapter you have the ability to perform operations on items such as:

- Network Topology
- Traffic

## Additional Product Documentation
The [API documents for Cisco CML](https://www.cisco.com/c/en/us/td/docs/cloud_services/cisco_modeling_labs/v110/installation/guide/admin/b_cml_install_sys_admin_11/b_cml_install_sys_admin_11_chapter_0111.pdf)