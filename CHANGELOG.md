
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:36PM

See merge request itentialopensource/adapters/adapter-cisco_cml!14

---

## 0.4.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-cisco_cml!12

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:51PM

See merge request itentialopensource/adapters/adapter-cisco_cml!11

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:04PM

See merge request itentialopensource/adapters/adapter-cisco_cml!10

---

## 0.4.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_cml!9

---

## 0.3.4 [03-27-2024]

* Changes made at 2024.03.27_14:05PM

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_cml!8

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_15:06PM

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_cml!7

---

## 0.3.2 [03-13-2024]

* Changes made at 2024.03.13_11:12AM

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_cml!6

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:11AM

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_cml!4

---

## 0.3.0 [12-31-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_cml!3

---

## 0.2.0 [05-23-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_cml!2

---

## 0.1.2 [09-07-2021]

- Authentication and security fixes

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_cml!1

---

## 0.1.1 [03-23-2021]

- Initial Commit

See commit d34aa8c

---
