/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-cisco_cml',
      type: 'CiscoCml',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const CiscoCml = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Cisco_cml Adapter Test', () => {
  describe('CiscoCml Class Tests', () => {
    const a = new CiscoCml(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('cisco_cml'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('cisco_cml'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('CiscoCml', pronghornDotJson.export);
          assert.equal('Cisco_cml', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-cisco_cml', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('cisco_cml'));
          assert.equal('CiscoCml', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-cisco_cml', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-cisco_cml', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#simpleUiUiAuthenticationAuthenticate - errors', () => {
      it('should have a simpleUiUiAuthenticationAuthenticate function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiUiAuthenticationAuthenticate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersUserRolesGet - errors', () => {
      it('should have a simpleUiHttpHandlersUserRolesGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersUserRolesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersUserListGet - errors', () => {
      it('should have a simpleUiHttpHandlersUserListGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersUserListGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersUserPasswordPut - errors', () => {
      it('should have a simpleUiHttpHandlersUserPasswordPut function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersUserPasswordPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.simpleUiHttpHandlersUserPasswordPut(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersUserPasswordPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersUserInfoGet - errors', () => {
      it('should have a simpleUiHttpHandlersUserInfoGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersUserInfoGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.simpleUiHttpHandlersUserInfoGet(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersUserInfoGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersUserDelete - errors', () => {
      it('should have a simpleUiHttpHandlersUserDelete function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersUserDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.simpleUiHttpHandlersUserDelete(null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersUserDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersUserPost - errors', () => {
      it('should have a simpleUiHttpHandlersUserPost function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersUserPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.simpleUiHttpHandlersUserPost(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersUserPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingStatusGet - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingStatusGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingStatusGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingTechSupportGet - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingTechSupportGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingTechSupportGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingStatusPut - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingStatusPut function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingStatusPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingTransportPut - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingTransportPut function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingTransportPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingDevCaPost - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingDevCaPost function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingDevCaPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingDevCaDelete - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingDevCaDelete function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingDevCaDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingRegistrationPost - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingRegistrationPost function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingRegistrationPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingRegistrationPut - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingRegistrationPut function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingRegistrationPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingRegistrationDelete - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingRegistrationDelete function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingRegistrationDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingFeaturesGet - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingFeaturesGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingFeaturesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingFeaturesPatch - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingFeaturesPatch function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingFeaturesPatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingReservationModePut - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingReservationModePut function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingReservationModePut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingReservationRequestPost - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingReservationRequestPost function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingReservationRequestPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingReservationCompletePost - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingReservationCompletePost function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingReservationCompletePost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingReservationCancelDelete - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingReservationCancelDelete function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingReservationCancelDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingReservationReleaseDelete - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingReservationReleaseDelete function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingReservationReleaseDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingReservationDiscardDelete - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingReservationDiscardDelete function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingReservationDiscardDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingReservationConfirmationCodeGet - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingReservationConfirmationCodeGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingReservationConfirmationCodeGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingReservationConfirmationCodeDelete - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingReservationConfirmationCodeDelete function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingReservationConfirmationCodeDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingReservationReturnCodeGet - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingReservationReturnCodeGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingReservationReturnCodeGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLicensingReservationReturnCodeDelete - errors', () => {
      it('should have a simpleUiHttpHandlersLicensingReservationReturnCodeDelete function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLicensingReservationReturnCodeDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersSystemArchive - errors', () => {
      it('should have a simpleUiHttpHandlersSystemArchive function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersSystemArchive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersClearBackupsAndShutdown - errors', () => {
      it('should have a simpleUiHttpHandlersClearBackupsAndShutdown function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersClearBackupsAndShutdown === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeDefinitionsGetListUiTransformed - errors', () => {
      it('should have a simpleUiHttpHandlersNodeDefinitionsGetListUiTransformed function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeDefinitionsGetListUiTransformed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersWaitForLldConnected - errors', () => {
      it('should have a simpleUiHttpHandlersWaitForLldConnected function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersWaitForLldConnected === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeDefinitionSchema - errors', () => {
      it('should have a simpleUiHttpHandlersNodeDefinitionSchema function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeDefinitionSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLabSchema - errors', () => {
      it('should have a simpleUiHttpHandlersLabSchema function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLabSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersFeedbackHandler - errors', () => {
      it('should have a simpleUiHttpHandlersFeedbackHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersFeedbackHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersSystemStatsHandler - errors', () => {
      it('should have a simpleUiHttpHandlersSystemStatsHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersSystemStatsHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersDiagnosticsHandler - errors', () => {
      it('should have a simpleUiHttpHandlersDiagnosticsHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersDiagnosticsHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersAuthOk - errors', () => {
      it('should have a simpleUiHttpHandlersAuthOk function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersAuthOk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetWebSessionTimeoutHandler - errors', () => {
      it('should have a simpleUiHttpHandlersGetWebSessionTimeoutHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetWebSessionTimeoutHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersSetWebSessionTimeoutHandler - errors', () => {
      it('should have a simpleUiHttpHandlersSetWebSessionTimeoutHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersSetWebSessionTimeoutHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeout', (done) => {
        try {
          a.simpleUiHttpHandlersSetWebSessionTimeoutHandler(null, (data, error) => {
            try {
              const displayE = 'timeout is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersSetWebSessionTimeoutHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetMacAddressBlockHandler - errors', () => {
      it('should have a simpleUiHttpHandlersGetMacAddressBlockHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetMacAddressBlockHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersSetMacAddressBlockHandler - errors', () => {
      it('should have a simpleUiHttpHandlersSetMacAddressBlockHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersSetMacAddressBlockHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing block', (done) => {
        try {
          a.simpleUiHttpHandlersSetMacAddressBlockHandler(null, (data, error) => {
            try {
              const displayE = 'block is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersSetMacAddressBlockHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersSystemInformationHandler - errors', () => {
      it('should have a simpleUiHttpHandlersSystemInformationHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersSystemInformationHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersWebsocketStatusHandler - errors', () => {
      it('should have a simpleUiHttpHandlersWebsocketStatusHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersWebsocketStatusHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersConvergedLabHandler - errors', () => {
      it('should have a simpleUiHttpHandlersConvergedLabHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersConvergedLabHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersConvergedLabHandler(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersConvergedLabHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersPostNodeHandler - errors', () => {
      it('should have a simpleUiHttpHandlersPostNodeHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersPostNodeHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersPostNodeHandler(null, null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersPostNodeHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLabSimStateHandler - errors', () => {
      it('should have a simpleUiHttpHandlersLabSimStateHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLabSimStateHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersLabSimStateHandler(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLabSimStateHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersSimulationStatsHandler - errors', () => {
      it('should have a simpleUiHttpHandlersSimulationStatsHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersSimulationStatsHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersSimulationStatsHandler(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersSimulationStatsHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLabElementStateHandler - errors', () => {
      it('should have a simpleUiHttpHandlersLabElementStateHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLabElementStateHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersLabElementStateHandler(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLabElementStateHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersSampleLabLoad - errors', () => {
      it('should have a simpleUiHttpHandlersSampleLabLoad function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersSampleLabLoad === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersSampleLabLoad(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersSampleLabLoad', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLabEventHandler - errors', () => {
      it('should have a simpleUiHttpHandlersLabEventHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLabEventHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersLabEventHandler(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLabEventHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLabListExtConnHandler - errors', () => {
      it('should have a simpleUiHttpHandlersLabListExtConnHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLabListExtConnHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersLabListExtConnHandler(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLabListExtConnHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersUpdateTopologyHandler - errors', () => {
      it('should have a simpleUiHttpHandlersUpdateTopologyHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersUpdateTopologyHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersUpdateTopologyHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersUpdateTopologyHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetPyatsTestbed - errors', () => {
      it('should have a simpleUiHttpHandlersGetPyatsTestbed function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetPyatsTestbed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersGetPyatsTestbed(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetPyatsTestbed', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLabAllocateAndStart - errors', () => {
      it('should have a simpleUiHttpHandlersLabAllocateAndStart function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLabAllocateAndStart === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersLabAllocateAndStart(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLabAllocateAndStart', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLabStop - errors', () => {
      it('should have a simpleUiHttpHandlersLabStop function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLabStop === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersLabStop(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLabStop', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLabUndo - errors', () => {
      it('should have a simpleUiHttpHandlersLabUndo function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLabUndo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersLabUndo(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLabUndo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLabWipe - errors', () => {
      it('should have a simpleUiHttpHandlersLabWipe function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLabWipe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersLabWipe(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLabWipe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersCreateLabHandler - errors', () => {
      it('should have a simpleUiHttpHandlersCreateLabHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersCreateLabHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetLabListHandler - errors', () => {
      it('should have a simpleUiHttpHandlersGetLabListHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetLabListHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetLabInfoHandler - errors', () => {
      it('should have a simpleUiHttpHandlersGetLabInfoHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetLabInfoHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersGetLabInfoHandler(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetLabInfoHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersRemoveLabHandler - errors', () => {
      it('should have a simpleUiHttpHandlersRemoveLabHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersRemoveLabHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersRemoveLabHandler(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersRemoveLabHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersPopulateLabsHandler - errors', () => {
      it('should have a simpleUiHttpHandlersPopulateLabsHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersPopulateLabsHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetLabTopologyHandler - errors', () => {
      it('should have a simpleUiHttpHandlersGetLabTopologyHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetLabTopologyHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersGetLabTopologyHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetLabTopologyHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetLabTileHandler - errors', () => {
      it('should have a simpleUiHttpHandlersGetLabTileHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetLabTileHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersGetLabTileHandler(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetLabTileHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersImportNgHandler - errors', () => {
      it('should have a simpleUiHttpHandlersImportNgHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersImportNgHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersImport1dotxHandler - errors', () => {
      it('should have a simpleUiHttpHandlersImport1dotxHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersImport1dotxHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersDownloadLabHandler - errors', () => {
      it('should have a simpleUiHttpHandlersDownloadLabHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersDownloadLabHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersDownloadLabHandler(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersDownloadLabHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersPutLabNotesHandler - errors', () => {
      it('should have a simpleUiHttpHandlersPutLabNotesHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersPutLabNotesHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersPutLabNotesHandler(null, null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersPutLabNotesHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersPutLabTitleHandler - errors', () => {
      it('should have a simpleUiHttpHandlersPutLabTitleHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersPutLabTitleHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersPutLabTitleHandler(null, null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersPutLabTitleHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersPutLabDescriptionHandler - errors', () => {
      it('should have a simpleUiHttpHandlersPutLabDescriptionHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersPutLabDescriptionHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersPutLabDescriptionHandler(null, null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersPutLabDescriptionHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersPostInterfaceHandler - errors', () => {
      it('should have a simpleUiHttpHandlersPostInterfaceHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersPostInterfaceHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersPostInterfaceHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersPostInterfaceHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersPostLinkHandler - errors', () => {
      it('should have a simpleUiHttpHandlersPostLinkHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersPostLinkHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersPostLinkHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersPostLinkHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersConvergedNodeHandler - errors', () => {
      it('should have a simpleUiHttpHandlersConvergedNodeHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersConvergedNodeHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersConvergedNodeHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersConvergedNodeHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersConvergedNodeHandler('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersConvergedNodeHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersSampleLabList - errors', () => {
      it('should have a simpleUiHttpHandlersSampleLabList function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersSampleLabList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeDefinitionGetList - errors', () => {
      it('should have a simpleUiHttpHandlersNodeDefinitionGetList function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeDefinitionGetList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetConsoleKeyForLabNode - errors', () => {
      it('should have a simpleUiHttpHandlersGetConsoleKeyForLabNode function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetConsoleKeyForLabNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersGetConsoleKeyForLabNode(null, null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetConsoleKeyForLabNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersGetConsoleKeyForLabNode('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetConsoleKeyForLabNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetVncKeyForLabNode - errors', () => {
      it('should have a simpleUiHttpHandlersGetVncKeyForLabNode function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetVncKeyForLabNode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersGetVncKeyForLabNode(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetVncKeyForLabNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersGetVncKeyForLabNode('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetVncKeyForLabNode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetLabNodesHandler - errors', () => {
      it('should have a simpleUiHttpHandlersGetLabNodesHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetLabNodesHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersGetLabNodesHandler(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetLabNodesHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetNodeStateHandler - errors', () => {
      it('should have a simpleUiHttpHandlersGetNodeStateHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetNodeStateHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersGetNodeStateHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetNodeStateHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersGetNodeStateHandler('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetNodeStateHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersStartNodeHandler - errors', () => {
      it('should have a simpleUiHttpHandlersStartNodeHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersStartNodeHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersStartNodeHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersStartNodeHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersStartNodeHandler('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersStartNodeHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersStopNodeHandler - errors', () => {
      it('should have a simpleUiHttpHandlersStopNodeHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersStopNodeHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersStopNodeHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersStopNodeHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersStopNodeHandler('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersStopNodeHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersWipeNodeHandler - errors', () => {
      it('should have a simpleUiHttpHandlersWipeNodeHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersWipeNodeHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersWipeNodeHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersWipeNodeHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersWipeNodeHandler('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersWipeNodeHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersExtractNodeConfigurationHandler - errors', () => {
      it('should have a simpleUiHttpHandlersExtractNodeConfigurationHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersExtractNodeConfigurationHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersExtractNodeConfigurationHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersExtractNodeConfigurationHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersExtractNodeConfigurationHandler('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersExtractNodeConfigurationHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeConfigGet - errors', () => {
      it('should have a simpleUiHttpHandlersNodeConfigGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeConfigGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeConfigGet(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeConfigGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeConfigGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeConfigGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeConfigPut - errors', () => {
      it('should have a simpleUiHttpHandlersNodeConfigPut function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeConfigPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeConfigPut(null, null, null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeConfigPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeConfigPut('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeConfigPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeTagsGet - errors', () => {
      it('should have a simpleUiHttpHandlersNodeTagsGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeTagsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeTagsGet(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeTagsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeTagsGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeTagsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeTagPost - errors', () => {
      it('should have a simpleUiHttpHandlersNodeTagPost function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeTagPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeTagPost(null, null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeTagPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeTagPost('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeTagPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tag', (done) => {
        try {
          a.simpleUiHttpHandlersNodeTagPost('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'tag is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeTagPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeTagDelete - errors', () => {
      it('should have a simpleUiHttpHandlersNodeTagDelete function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeTagDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeTagDelete(null, null, null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeTagDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeTagDelete('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeTagDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tag', (done) => {
        try {
          a.simpleUiHttpHandlersNodeTagDelete('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tag is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeTagDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeImageDefinitionPut - errors', () => {
      it('should have a simpleUiHttpHandlersNodeImageDefinitionPut function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeImageDefinitionPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeImageDefinitionPut(null, null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeImageDefinitionPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeImageDefinitionPut('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeImageDefinitionPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeViewGet - errors', () => {
      it('should have a simpleUiHttpHandlersNodeViewGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeViewGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeViewGet(null, null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeViewGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeViewGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeViewGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeViewPatch - errors', () => {
      it('should have a simpleUiHttpHandlersNodeViewPatch function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeViewPatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeViewPatch(null, null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeViewPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeViewPatch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeViewPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeViewDelete - errors', () => {
      it('should have a simpleUiHttpHandlersNodeViewDelete function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeViewDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeViewDelete(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeViewDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeViewDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeViewDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetLabNodeLayer3AddressesHandler - errors', () => {
      it('should have a simpleUiHttpHandlersGetLabNodeLayer3AddressesHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetLabNodeLayer3AddressesHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersGetLabNodeLayer3AddressesHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetLabNodeLayer3AddressesHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersGetLabNodeLayer3AddressesHandler('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetLabNodeLayer3AddressesHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersConvergedLinkHandler - errors', () => {
      it('should have a simpleUiHttpHandlersConvergedLinkHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersConvergedLinkHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersConvergedLinkHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersConvergedLinkHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.simpleUiHttpHandlersConvergedLinkHandler('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersConvergedLinkHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetPcapKeyForLabLink - errors', () => {
      it('should have a simpleUiHttpHandlersGetPcapKeyForLabLink function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetPcapKeyForLabLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersGetPcapKeyForLabLink(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetPcapKeyForLabLink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.simpleUiHttpHandlersGetPcapKeyForLabLink('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetPcapKeyForLabLink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersPutLinkCaptureStartHandler - errors', () => {
      it('should have a simpleUiHttpHandlersPutLinkCaptureStartHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersPutLinkCaptureStartHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersPutLinkCaptureStartHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersPutLinkCaptureStartHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.simpleUiHttpHandlersPutLinkCaptureStartHandler('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersPutLinkCaptureStartHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersPutLinkCaptureStopHandler - errors', () => {
      it('should have a simpleUiHttpHandlersPutLinkCaptureStopHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersPutLinkCaptureStopHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersPutLinkCaptureStopHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersPutLinkCaptureStopHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.simpleUiHttpHandlersPutLinkCaptureStopHandler('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersPutLinkCaptureStopHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersPutLinkStartHandler - errors', () => {
      it('should have a simpleUiHttpHandlersPutLinkStartHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersPutLinkStartHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersPutLinkStartHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersPutLinkStartHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.simpleUiHttpHandlersPutLinkStartHandler('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersPutLinkStartHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersPutLinkStopHandler - errors', () => {
      it('should have a simpleUiHttpHandlersPutLinkStopHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersPutLinkStopHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersPutLinkStopHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersPutLinkStopHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.simpleUiHttpHandlersPutLinkStopHandler('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersPutLinkStopHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLinkViewGet - errors', () => {
      it('should have a simpleUiHttpHandlersLinkViewGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLinkViewGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersLinkViewGet(null, null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLinkViewGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.simpleUiHttpHandlersLinkViewGet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLinkViewGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLinkViewPut - errors', () => {
      it('should have a simpleUiHttpHandlersLinkViewPut function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLinkViewPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersLinkViewPut(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLinkViewPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.simpleUiHttpHandlersLinkViewPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLinkViewPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLinkViewPatch - errors', () => {
      it('should have a simpleUiHttpHandlersLinkViewPatch function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLinkViewPatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersLinkViewPatch(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLinkViewPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.simpleUiHttpHandlersLinkViewPatch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLinkViewPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersLinkViewDelete - errors', () => {
      it('should have a simpleUiHttpHandlersLinkViewDelete function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersLinkViewDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersLinkViewDelete(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLinkViewDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkId', (done) => {
        try {
          a.simpleUiHttpHandlersLinkViewDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersLinkViewDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeDefinitionsPost - errors', () => {
      it('should have a simpleUiHttpHandlersNodeDefinitionsPost function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeDefinitionsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeDefinitionsPut - errors', () => {
      it('should have a simpleUiHttpHandlersNodeDefinitionsPut function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeDefinitionsPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeDefinitionsGet - errors', () => {
      it('should have a simpleUiHttpHandlersNodeDefinitionsGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeDefinitionsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeDefinitionsGet(null, null, (data, error) => {
            try {
              const displayE = 'defId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeDefinitionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeDefinitionsDelete - errors', () => {
      it('should have a simpleUiHttpHandlersNodeDefinitionsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeDefinitionsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeDefinitionsDelete(null, (data, error) => {
            try {
              const displayE = 'defId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeDefinitionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetLabLayer3AddressesHandler - errors', () => {
      it('should have a simpleUiHttpHandlersGetLabLayer3AddressesHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetLabLayer3AddressesHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersGetLabLayer3AddressesHandler(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetLabLayer3AddressesHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeImageDefinitionGet - errors', () => {
      it('should have a simpleUiHttpHandlersNodeImageDefinitionGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeImageDefinitionGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeImageDefinitionGet(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeImageDefinitionGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeImageDefinitionGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeImageDefinitionGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersBuildConfigurationsManager - errors', () => {
      it('should have a simpleUiHttpHandlersBuildConfigurationsManager function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersBuildConfigurationsManager === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersBuildConfigurationsManager(null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersBuildConfigurationsManager', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeByLabelGet - errors', () => {
      it('should have a simpleUiHttpHandlersNodeByLabelGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeByLabelGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeByLabelGet(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeByLabelGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing searchQuery', (done) => {
        try {
          a.simpleUiHttpHandlersNodeByLabelGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'searchQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeByLabelGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersNodeTagFindall - errors', () => {
      it('should have a simpleUiHttpHandlersNodeTagFindall function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersNodeTagFindall === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersNodeTagFindall(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeTagFindall', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing searchQuery', (done) => {
        try {
          a.simpleUiHttpHandlersNodeTagFindall('fakeparam', null, (data, error) => {
            try {
              const displayE = 'searchQuery is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersNodeTagFindall', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersTelemetryDataHandler - errors', () => {
      it('should have a simpleUiHttpHandlersTelemetryDataHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersTelemetryDataHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersDiagnosticEventDataHandler - errors', () => {
      it('should have a simpleUiHttpHandlersDiagnosticEventDataHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersDiagnosticEventDataHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersSystemHealthGet - errors', () => {
      it('should have a simpleUiHttpHandlersSystemHealthGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersSystemHealthGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetInterfaceStateHandler - errors', () => {
      it('should have a simpleUiHttpHandlersGetInterfaceStateHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetInterfaceStateHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersGetInterfaceStateHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetInterfaceStateHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.simpleUiHttpHandlersGetInterfaceStateHandler('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetInterfaceStateHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetNodeInterfacesHandler - errors', () => {
      it('should have a simpleUiHttpHandlersGetNodeInterfacesHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetNodeInterfacesHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersGetNodeInterfacesHandler(null, null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetNodeInterfacesHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeId', (done) => {
        try {
          a.simpleUiHttpHandlersGetNodeInterfacesHandler('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersGetNodeInterfacesHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersStopInterfaceHandler - errors', () => {
      it('should have a simpleUiHttpHandlersStopInterfaceHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersStopInterfaceHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersStopInterfaceHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersStopInterfaceHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.simpleUiHttpHandlersStopInterfaceHandler('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersStopInterfaceHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersStartInterfaceHandler - errors', () => {
      it('should have a simpleUiHttpHandlersStartInterfaceHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersStartInterfaceHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersStartInterfaceHandler(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersStartInterfaceHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.simpleUiHttpHandlersStartInterfaceHandler('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersStartInterfaceHandler', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersInterfaceViewGet - errors', () => {
      it('should have a simpleUiHttpHandlersInterfaceViewGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersInterfaceViewGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersInterfaceViewGet(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersInterfaceViewGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.simpleUiHttpHandlersInterfaceViewGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersInterfaceViewGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersInterfaceViewPut - errors', () => {
      it('should have a simpleUiHttpHandlersInterfaceViewPut function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersInterfaceViewPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersInterfaceViewPut(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersInterfaceViewPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.simpleUiHttpHandlersInterfaceViewPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersInterfaceViewPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersInterfaceViewPatch - errors', () => {
      it('should have a simpleUiHttpHandlersInterfaceViewPatch function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersInterfaceViewPatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersInterfaceViewPatch(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersInterfaceViewPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.simpleUiHttpHandlersInterfaceViewPatch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersInterfaceViewPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersInterfaceViewDelete - errors', () => {
      it('should have a simpleUiHttpHandlersInterfaceViewDelete function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersInterfaceViewDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labId', (done) => {
        try {
          a.simpleUiHttpHandlersInterfaceViewDelete(null, null, (data, error) => {
            try {
              const displayE = 'labId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersInterfaceViewDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.simpleUiHttpHandlersInterfaceViewDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersInterfaceViewDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersImageDefinitionsForNdIdGet - errors', () => {
      it('should have a simpleUiHttpHandlersImageDefinitionsForNdIdGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersImageDefinitionsForNdIdGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defId', (done) => {
        try {
          a.simpleUiHttpHandlersImageDefinitionsForNdIdGet(null, (data, error) => {
            try {
              const displayE = 'defId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersImageDefinitionsForNdIdGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersImageDefinitionDropfolderListGet - errors', () => {
      it('should have a simpleUiHttpHandlersImageDefinitionDropfolderListGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersImageDefinitionDropfolderListGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersImageDefinitionsList - errors', () => {
      it('should have a simpleUiHttpHandlersImageDefinitionsList function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersImageDefinitionsList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersImageDefinitionsPost - errors', () => {
      it('should have a simpleUiHttpHandlersImageDefinitionsPost function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersImageDefinitionsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersImageDefinitionsPut - errors', () => {
      it('should have a simpleUiHttpHandlersImageDefinitionsPut function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersImageDefinitionsPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersImageDefinitionsGet - errors', () => {
      it('should have a simpleUiHttpHandlersImageDefinitionsGet function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersImageDefinitionsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defId', (done) => {
        try {
          a.simpleUiHttpHandlersImageDefinitionsGet(null, null, (data, error) => {
            try {
              const displayE = 'defId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersImageDefinitionsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersImageDefinitionsDelete - errors', () => {
      it('should have a simpleUiHttpHandlersImageDefinitionsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersImageDefinitionsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defId', (done) => {
        try {
          a.simpleUiHttpHandlersImageDefinitionsDelete(null, (data, error) => {
            try {
              const displayE = 'defId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersImageDefinitionsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersDiskImageUpload - errors', () => {
      it('should have a simpleUiHttpHandlersDiskImageUpload function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersDiskImageUpload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersImageDefinitionDropfolderRemove - errors', () => {
      it('should have a simpleUiHttpHandlersImageDefinitionDropfolderRemove function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersImageDefinitionDropfolderRemove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filename', (done) => {
        try {
          a.simpleUiHttpHandlersImageDefinitionDropfolderRemove(null, (data, error) => {
            try {
              const displayE = 'filename is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_cml-adapter-simpleUiHttpHandlersImageDefinitionDropfolderRemove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetAllConsoleKeysHandler - errors', () => {
      it('should have a simpleUiHttpHandlersGetAllConsoleKeysHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetAllConsoleKeysHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simpleUiHttpHandlersGetAllVncKeysHandler - errors', () => {
      it('should have a simpleUiHttpHandlersGetAllVncKeysHandler function', (done) => {
        try {
          assert.equal(true, typeof a.simpleUiHttpHandlersGetAllVncKeysHandler === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
